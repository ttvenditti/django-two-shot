from django.urls import include, path
from receipts.views import receipts_view, create_receipt, category_views, accounts_views, create_category, create_account

urlpatterns = [
path("", receipts_view, name = "home"),
path("create/", create_receipt, name = "create_receipt"),
path("categories/", category_views, name = "category_views"),
path("accounts/", accounts_views, name = "accounts_views"),
path("categories/create/", create_category, name = "create_category"),
path("accounts/create/", create_account, name = "create_account"),
]
