from django.shortcuts import render
from django.shortcuts import redirect, render, get_object_or_404
from receipts.models import Receipt,  ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm

# Create your views here.

@login_required
def receipts_view(request):
    rv = Receipt.objects.filter(purchaser=request.user)
    context = {
        "rv": rv
    }
    return render(request, "receipts/receipts.html", context)

@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)


@login_required
def category_views(request):
    cv = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "cv": cv,
    }
    return render(request, "receipts/categories.html", context)

@login_required
def accounts_views(request):
    av = Account.objects.filter(owner=request.user)
    context = {
        "av": av,
    }
    return render(request, "receipts/accounts.html", context)

@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(commit=False)
            category.owner = request.user
            category.save()
            return redirect("category_views")
    else:
        form = ExpenseCategoryForm()
    context = {
        "form": form,
    }
    return render(request, "categories/create.html", context)

@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(commit=False)
            account.owner = request.user
            account.save()
            return redirect("accounts_views")
    else:
        form = AccountForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/create.html", context)
