from django.contrib import admin
from receipts.models import Receipt, ExpenseCategory, Account

# Register your models here.
@admin.register(Receipt)
class Receipt(admin.ModelAdmin):
    list_display = [
        "vendor",
        "total",
        "tax",
    ]


@admin.register(Account)
class Account(admin.ModelAdmin):
    list_display = [
        "name",
        "number",
        "id"

    ]


@admin.register(ExpenseCategory)
class ExpenseCategory(admin.ModelAdmin):
    list_display = [
        "name"
    ]
